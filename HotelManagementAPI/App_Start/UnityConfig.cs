using BusinessLayer;
using DatabaseLayer;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace HotelManagementAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            container.RegisterType<IBusinessLayerCustomer, CustomerBusiness>();
            container.RegisterType<IAddHotelDataLayer, AddHotelDataLayer>();
            container.RegisterType<IAddHotelBusiness, AddHotelBusiness>();
            container.RegisterType<IDataLayerInterface, CustomerData>();
            container.RegisterType<IAddRoomsBusiness, AddRoomsBusiness>();
            container.RegisterType<IAddRoomsDatalayer, AddRoomsDatalayer>();
            container.RegisterType<IGetAllHotelsBusiness, GetAllHotelsBusiness>();
            container.RegisterType<IGetAllHotelsDataLayer, GetAllHotelsDataLayer>();
            container.RegisterType<IGetRoomsBusiness, GetRoomsBusiness>();
            container.RegisterType<IGetRoomsDataLayer, GetRoomsDataLayer>();
            container.RegisterType<ICheckAvailibilityOfRoomBusiness, CheckAvailibilityOfRoomBusiness>();
            container.RegisterType<ICheckAvailibilityOfRoomDataLayer, CheckAvailibilityOfRoomDataLayer>();
            container.RegisterType<IBookingRoomDataLayer, BookingRoomDatalayer>();
            container.RegisterType<IBookingRoomBusiness, BookingRoomBusiness>();
            container.RegisterType<IUpdateBookingDateBusiness, UpdateBookingDateBusiness>();
            container.RegisterType<IUpdateBookingDateDataLayer, UpdateBookingDateDataLayer>();
            container.RegisterType<IDeleteBookingBusiness, DeleteBookingBusiness>();
            container.RegisterType<IDeleteBookingDataLayer, DeleteBooingDataLayer>();
            container.RegisterType<IRoomDetailsDataLayer, RoomDetailsDatabase>();
            container.RegisterType<IRoomDetailsBusiness, RoomDetailsBusiness>();



            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}