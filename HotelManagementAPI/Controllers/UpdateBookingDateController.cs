﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{

    [BasicAuthentication]
    public class UpdateBookingDateController : ApiController
    {
        private IUpdateBookingDateBusiness updateBookingDateBusiness;
        public UpdateBookingDateController(IUpdateBookingDateBusiness _updateBookingDateBusiness)
        {
            updateBookingDateBusiness = _updateBookingDateBusiness;
        }

        [Route("api/UpdateBookingDate/")]

        [HttpPut]
        public IHttpActionResult UpdateBookingDate(int id, BookingViewModel bookingVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bookingVM.BookingId)
            {
                return BadRequest();
            }

            bool var = updateBookingDateBusiness.UpdateBookingBusiness(id, bookingVM);

            if (!var)
            {
                var error = "Room Not Exist!";
                return Ok(error) ;
            }
            return Ok(var);
        }
    }
}
