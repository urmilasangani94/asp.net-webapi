﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;

namespace HotelManagementAPI.Controllers
{

    [BasicAuthentication]
    public class BookingRoomController : ApiController
    {
        IBookingRoomBusiness bookingRoomBusiness;
        public BookingRoomController(IBookingRoomBusiness _bookingRoomBusiness)
        {
            bookingRoomBusiness = _bookingRoomBusiness;
        }
        [Route("api/BookingRoom")]
        [HttpPost]
        public IHttpActionResult BookRoom(BookingViewModel bookingVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var booked = bookingRoomBusiness.AllReadyBooked(bookingVM);
            if(booked == false)
            { 
            var Available = bookingRoomBusiness.BookingRoom(bookingVM);
                if (Available == false)
                {

                    var error = "Room is Not Exist!";
                    return Ok(error);
                }
            }
            if(booked == true)
            {
                var error = "Room All Ready Booked!";
                return Ok(error);
            }

            //return Ok(itemObject);
            return CreatedAtRoute("DefaultApi", new { id = bookingVM.BookingId }, bookingVM);

        }

    }
}
