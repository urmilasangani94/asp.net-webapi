﻿using BusinessLayer;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{

    [BasicAuthentication]
    public class CheckAvailibilityOfRoomController : ApiController
    {

        private ICheckAvailibilityOfRoomBusiness checkAvailibilityOfRoomBusiness;
        public CheckAvailibilityOfRoomController(ICheckAvailibilityOfRoomBusiness _checkAvailibilityOfRoomBusiness)
        {
            checkAvailibilityOfRoomBusiness = _checkAvailibilityOfRoomBusiness;
        }
        [HttpGet]
        [Route("api/CheckAvailibilityOfRoom/")]
        public IHttpActionResult CheckAvailableRoom(int roomid, DateTime date)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            bool isAvailabe = checkAvailibilityOfRoomBusiness.AvailableRoomBusiness(roomid, date);
            return Ok(isAvailabe);

        }

    }
}
