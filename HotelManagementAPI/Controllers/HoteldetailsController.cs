﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace HotelManagementAPI.Controllers
{
    [BasicAuthentication]
    public class HoteldetailsController : ApiController
    {
        private IBusinessLayerCustomer Ibusinesslayercustomer;
        public HoteldetailsController(IBusinessLayerCustomer businessLayerCustomer)
        {
            Ibusinesslayercustomer = businessLayerCustomer;
        }

        [Route("api/Hoteldetails")]
        public IHttpActionResult PostHotels(HoteldetailsViewModel hoteldetailsViewModel)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Object hotelList= Ibusinesslayercustomer.FindHotelBusiness(hoteldetailsViewModel);
            var itemObject = JsonConvert.SerializeObject(hotelList,
                   Formatting.None,
                   new JsonSerializerSettings()
                  {
                      ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                  });
           // var array = hotelList as string[];
            if (hotelList == null)
            {
                return Content(HttpStatusCode.BadRequest, "Hotel is Not available for your required date!");
            }

            return Ok(itemObject);
        }

    }
}

