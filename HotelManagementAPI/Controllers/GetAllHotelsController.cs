﻿using BusinessLayer;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{

    [BasicAuthentication]
    public class GetAllHotelsController : ApiController
    {
        private IGetAllHotelsBusiness getAllHotelsBusiness ;
        public GetAllHotelsController(IGetAllHotelsBusiness _getAllHotelsBusiness)
        {
            getAllHotelsBusiness = _getAllHotelsBusiness;
        }

        [Route("api/GetAllHotels")]
        public IHttpActionResult GetHotels()
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Hotels = getAllHotelsBusiness.FindHotelData();

            return Ok(Hotels);
        }
    }
}
