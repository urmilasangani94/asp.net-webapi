﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace HotelManagementAPI.Controllers
{
    public class LoginController : ApiController
    {
        private IBusinessLayerCustomer Ibusinesslayercustomer;
        public LoginController(IBusinessLayerCustomer businessLayerCustomer)
        {
            Ibusinesslayercustomer = businessLayerCustomer;
        }

        [Route("api/Login")]
        public IHttpActionResult PostCustomer(LoginViewModel customer)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Isexist = Ibusinesslayercustomer.LogincustomerBusiness(customer);
            if (Isexist == false)
            {
                return Content(HttpStatusCode.BadRequest, "Your EmailId or Password is wrong");
              
            }
            return Ok(Isexist);
        }

    }
}

